import java.util.Iterator;
import java.util.LinkedList;

public class LongStack {

    private LinkedList<Long> stack;

    public static void main(String[] argum) {

    }

    LongStack() {
        stack = new LinkedList<>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LongStack newStack = new LongStack();
        newStack.setStack(new LinkedList<>(stack));
        return newStack;
    }

    public boolean stEmpty() {
        return stack.isEmpty();
    }

    public void push(long a) {
        stack.push(a);
    }

    public long pop() {
        if (!stack.isEmpty()) {
            return stack.pop();
        } else {
            throw new RuntimeException("Cannot pop from empty stack");
        }
    }

    public void op(String s) {
        if (stack.size() >= 2) {
            if (s == null || s.isEmpty()) {
                throw new RuntimeException("String '" + s + "'is empty or null, please provide an operator");
            } else {
                if (s.length() == 1) {
                    Long element2 = pop();
                    Long element1 = pop();
                    switch (s) {
                        case "+":
                            push(element1 + element2);
                            break;
                        case "-":
                            push(element1 - element2);
                            break;
                        case "*":
                            push(element1 * element2);
                            break;
                        case "/":
                            push(element1 / element2);
                            break;
                        default:
                            throw new RuntimeException("Illegal operator " + s);
                    }
                } else {
                    throw new RuntimeException("Please provide single element string operator: " + s);
                }
            }
        } else {
            throw new RuntimeException("Cannot do operation when element count is < 2");
        }
    }

    public long tos() {
        if (!stack.isEmpty()) {
            return stack.peek();
        } else {
            throw new RuntimeException("Cannot tos from empty stack");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof LongStack) {
            if (!(((LongStack) o).getStack().isEmpty() && stEmpty())) {
                if (((LongStack) o).getStack().size() == getStack().size()) {
                    if (compareElements(((LongStack) o).getStack())) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    private boolean compareElements(LinkedList<Long> stack) {
        Long element1 = stack.pop();
        Long element2 = getStack().pop();
        if (element1.equals(element2)) {
            if (stack.isEmpty()) {
                stack.push(element1);
                getStack().push(element2);
                return true;
            } else {
                return compareElements(stack);
            }
        } else {
            stack.push(element1);
            getStack().push(element2);
            return false;
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Iterator<Long> iterator = getStack().descendingIterator();
        while (iterator.hasNext()) {
            stringBuilder.append(iterator.next());
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    public static long interpret(String pol) {
        LongStack longStack = new LongStack();
        if (pol != null && !pol.isEmpty()) {
            String cleanPolishString = pol.replaceAll("[\t\n]", " ").trim();
            if (!cleanPolishString.isEmpty()) {
                String[] substringList = cleanPolishString.split("\\s+");
                for (String string : substringList) {
                    if (isNumeric(string)) {
                        longStack.push(Long.parseLong(string));
                    } else {
                        longStack.op(string);
                    }
                }
            }
        }
        if(longStack.getStack().size() != 1){
            throw new RuntimeException("Invalid Polish Notation String given: " + pol);
        }
        return longStack.pop();
    }

    private static boolean isNumeric(String stringToCheck) {
        try {
            Long.parseLong(stringToCheck);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public LinkedList<Long> getStack() {
        return stack;
    }

    public void setStack(LinkedList<Long> stack) {
        this.stack = stack;
    }
}

